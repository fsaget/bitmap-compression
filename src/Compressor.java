import java.awt.Color;

public class Compressor {
    /** 
     * Performs a delta compression on the given image. 
     * 
     * An end node is merged into a leaf if the euclidean 
     * distance between all of its leaves exceeds the 
     * given threshold.
     * 
     * @param img the quad-tree representation of an image
     * @param delta the threshold, 0 < delta < 192
     * @return the compressed image
    */
    public static QuadTree deltaCompress(QuadTree img, int delta) {
        if(img.isLeaf()) {
            //  if the node is a leaf, it cannot be compressed further
            return img;
        } else {
            //  compressing the subregions
            img.setNW(deltaCompress(img.getNW(), delta));
            img.setNE(deltaCompress(img.getNE(), delta));
            img.setSE(deltaCompress(img.getSE(), delta));
            img.setSW(deltaCompress(img.getSW(), delta));
            
            if(img.isEndNode() && getEuclideanDistance(img) <= delta) {
                //  merging the leaves into a leaf with average color
                return new QuadTree(img.getAverageColor());
            } else return img;			
        }
    }
    
    /**
     * Performs a phi compression on the given image.
     * 
     * The quad-tree is reduced so its total number of 
     * leaves is inferior to the given parameter. Leaves
     * are eliminated based on their euclidean distance 
     * to the average color of the node.
     * 
     * @param img the quad-tree representation of an image
     * @param phi the total number of leaves, delta > 0
     * @return the compressed image
     */
    public static QuadTree phiCompress(QuadTree img, int phi) {
        System.out.println("/!\\ Work in progress...");
        return img;
    }

    /**
     * Computes the euclidean distance for an end node.
     *  
     * In this context, the euclidean distance corresponds
     * to the color difference of each of the leaves of a 
     * node, meaning this calculation is only meaningful 
     * when applied to a end node (a QuadTree which all 4
     * children are leaves).
     * 
     * FIXME : this might be why the end result is not complete. 
     * Might want to allow non-end node, restricting to 3 or 2 leaves.
     * 
     * @param node an end node with 4 leaves
     * @return the max euclidean distance among all leaves
     * */ 
    private static double getEuclideanDistance(QuadTree node) {
        Color averageColor = node.getAverageColor();
		Double[] eucDist = new Double[4];	
		
        eucDist[0] = 
            Math.sqrt((
                Math.pow((node.getNW().getColor().getRed()    - averageColor.getRed()), 2) +
                Math.pow((node.getNW().getColor().getGreen()  - averageColor.getGreen()), 2) + 
                Math.pow((node.getNW().getColor().getBlue()   - averageColor.getBlue()), 2)) / 3f);
    
        eucDist[1] = 
        Math.sqrt((
            Math.pow((node.getNE().getColor().getRed()    - averageColor.getRed()), 2) +
            Math.pow((node.getNE().getColor().getGreen()  - averageColor.getGreen()), 2) + 
            Math.pow((node.getNE().getColor().getBlue()   - averageColor.getBlue()), 2)) / 3f);

        eucDist[2] = 
        Math.sqrt((
            Math.pow((node.getSE().getColor().getRed()    - averageColor.getRed()), 2) +
            Math.pow((node.getSE().getColor().getGreen()  - averageColor.getGreen()), 2) + 
            Math.pow((node.getSE().getColor().getBlue()   - averageColor.getBlue()), 2)) / 3f);

        eucDist[3] = 
        Math.sqrt((
            Math.pow((node.getSW().getColor().getRed()    - averageColor.getRed()), 2) +
            Math.pow((node.getSW().getColor().getGreen()  - averageColor.getGreen()), 2) + 
            Math.pow((node.getSW().getColor().getBlue()   - averageColor.getBlue()), 2)) / 3f);
		
		double max = 0;
		
		for(Double d : eucDist) {
			if(d > max) {
				max = d;
			}
		}
		
		return max;
    }
}
