import java.io.File;
import java.io.PrintWriter;
import java.io.IOException;
import java.util.Scanner;

public class Main {
	/**
	 * Interacts with the user to 
	 * get the path to an image.
	 * 
	 * @param s an opened scanner to get user input
	 * @return an image object
	 * @throws IOException if the image file cannot be opened
	 */
	private static ImagePNG promptForImage(Scanner s) throws IOException{
		String imgPath = s.nextLine();			

		System.out.println("Opening image...");
		ImagePNG img = new ImagePNG(imgPath);
		System.out.println("Image imported successfully.");

		return img;
	}

	/**
	 * Auxiliary functions to perform each feature.
	 */

	//	Tree saving
	private static void performSaveAsText(ImagePNG img) {
		QuadTree qt = new QuadTree(img);
		System.out.println("Saving quadtree as text file...");

		try(PrintWriter out = new PrintWriter("tree.txt")) {
			out.println(qt.toString());
			System.out.println("Sauvegarde de tree.txt effectuée !");
		} catch(Exception e) {
			System.out.println(e.getMessage());
			System.exit(1);
		}
	}

	private static void performSaveAsText(Scanner s) {
		try {
			System.out.println("Enter the path to the image to be saved as text : ");
			ImagePNG img = promptForImage(s);
			performSaveAsText(img);
		} catch(IOException e) {
			System.out.println("Couldn't open file, terminating program.");
			System.exit(1);
		}
	}

	//	Delta compression
	private static void performDeltaCompression(ImagePNG img, int delta) {
		QuadTree qt = new QuadTree(img);
		QuadTree dc = Compressor.deltaCompress(qt, delta);

		ImagePNG compressed = dc.toPNG(img.height());

		System.out.println("Compression was " + ImagePNG.computeEQM(img, compressed) + "% faithful.");

		try {
			System.out.println("Saving compressed image...");
			compressed.save("compressed.png");
		} catch(IOException e) {
			System.out.println("There was an error saving the image data.");
			System.exit(1);
		}
	}

	private static void performDeltaCompression(Scanner s) {
		try {
			int delta = -1;
			System.out.println("Enter the path to the image to be delta-compressed : ");
			ImagePNG img = promptForImage(s);

			System.out.println("Enter the delta coefficient : ");
			while(delta < 0 || delta > 192) {
				delta = s.nextInt();
				s.nextLine();
			}

			performDeltaCompression(img, delta);
		} catch(IOException e) {
			System.out.println("Couldn't open file, terminating program.");
			System.exit(1);
		}
	}
	
	//	Phi compression
	private static void performPhiCompression(ImagePNG img, int phi) {
		QuadTree qt = new QuadTree(img);
		QuadTree dc = Compressor.phiCompress(qt, phi);

		ImagePNG compressed = dc.toPNG(img.height());

		System.out.println("Compression was " + ImagePNG.computeEQM(img, compressed) + "% faithful.");

		try {
			System.out.println("Saving compressed image...");
			compressed.save("compressed.png");
		} catch(IOException e) {
			System.out.println("There was an error saving the image data.");
			System.exit(1);
		}
	}

	private static void performPhiCompression(Scanner s) {
		try {
			int phi = -1;
			System.out.println("Enter the path to the image to be phi-compressed : ");
			ImagePNG img = promptForImage(s);
	
			System.out.println("Enter the phi coefficient : ");
			while(phi < 0) {
				phi = s.nextInt();
				s.nextLine();
			}
	
			performPhiCompression(img, phi);
		} catch(IOException e) {
			System.out.println("Couldn't open file, terminating program.");
			System.exit(1);
		}
	}
	
	//	Image comparison
	private static void performComparison(ImagePNG img1, ImagePNG img2) {
		System.out.println("Images are " + ImagePNG.computeEQM(img1, img2) + "% similar.");
	}

	private static void performComparison(Scanner s) {
		try {
			System.out.println("Enter the path to the first image : ");
			ImagePNG img1 = promptForImage(s);
			System.out.println("Enter the path to the image it will be compared against : ");
			ImagePNG img2 = promptForImage(s);
			performComparison(img1, img2);
		} catch(IOException e) {
			System.out.println("Couldn't open file, terminating program.");
			System.exit(1);
		}
	}

	public static void main(String[] args) {
		if(args.length == 0) {
			//	INTERACTIVE MODE
			Scanner scn = new Scanner(System.in);
			int mode = 0;
			
			System.out.println("////////////////");
			System.out.println("/////    gitlab.com/fsaget");
			System.out.println("           PNG COMPRESSOR    //////////");
			System.out.println("                       ////////////////\n\n");
			System.out.println("Choose an option :");
			System.out.println("1 --- Save as text");
			System.out.println("2 --- Delta compression");
			System.out.println("3 --- Phi compression");
			System.out.println("4 --- Compare images");
			
			while(mode < 1 || mode > 4) {
				mode = scn.nextInt();
				scn.nextLine();
			}

			switch(mode) {
				case 1:
					performSaveAsText(scn);
					break;
				case 2:
					performDeltaCompression(scn);
					break;
				case 3:
					performPhiCompression(scn);
					break;
				case 4:
					performComparison(scn);
					break;
				default:
					//	should never happen
					System.out.println("Wrong mode selected, computer will now combust.");
					System.exit(1);
			}
			
			scn.close();
		} else if(args.length == 1) {
			/**
			 * Saving the QuadTree as text in the 
			 * specified text file.
			 */

			try {
				ImagePNG img = new ImagePNG(args[0]);
				performSaveAsText(img);
			} catch (IOException e) {
				System.out.println(e.getMessage());
				System.exit(1);
			}
		} else if(args.length == 3 && args[1] == "-d" || args[1] == "-delta") {
			/**
			 * Delta compression.
			 */

			int delta = Integer.parseInt(args[3]);
			
			try {
				ImagePNG img = new ImagePNG(args[0]);
				performDeltaCompression(img, delta);
			} catch (IOException e) {
				System.out.println(e.getMessage());
				System.exit(1);
			}
		} else if(args.length == 3 && args[1] == "-p" || args[1] == "-phi") {	
			/**
			 * Phi compression.
			 */

			int phi = Integer.parseInt(args[3]);
			
			try {
				ImagePNG img = new ImagePNG(args[0]);
				performPhiCompression(img, phi);
			} catch (IOException e) {
				System.out.println(e.getMessage());
				System.exit(1);
			}
		} else if ((args.length == 2)) {
			/**
			 * Comparison of the two image whose paths
			 * have been specified.
			 */
			
			try {
				ImagePNG img1 = new ImagePNG(args[0]);
				ImagePNG img2 = new ImagePNG(args[1]);
				performComparison(img1, img2);
			} catch (IOException e) {
				System.out.println(e.getMessage());
				System.exit(1);
			}
		} else {
			System.out.println("Invalid arguments.");
		}

		System.exit(0);
	}
}
