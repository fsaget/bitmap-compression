import java.awt.Color;
import java.util.ArrayList;

/**
 * An implementation of the R-quadtree data
 * structure. Represents an image by recursivly
 * subdividing it into four sub regions.
 */
public class QuadTree {
	private Color col;
	private int depth;
	private QuadTree[] quarters;
	
	///////////////////
	//	Constructors //

	/**
	 * Constructs a quad-tree representation
	 * of a given image, starting at a given
	 * position.
	 * 
	 * @param png the base image
	 * @param startX start x-value
	 * @param startY start y-value
	 * @param height side length of the region
	 */
	private QuadTree(ImagePNG png, int startX, int startY, int height) {
		this.quarters = new QuadTree[4];
		this.depth = (int)(Math.log(height) / Math.log(2));		//	logb(a) = log(a)/log(b)
		
		if(height == 1) {
			/**
			 * If the image consists of only one
			 * pixel, take the color of that pixel
			 * and make it a leaf.
			 */
			this.col = png.getPixel(startX, startY);
			
			for(int i = 0; i < 4; i++) {
				this.quarters[i] = null;
			}
		} else {
			this.col = null;

			/**
			 * The picture gets split into four 
			 * subregions which are recursively
			 * turned into trees.
			 */
			this.quarters[0] = new QuadTree(png, startX, startY, height/2);							// NW
			this.quarters[1] = new QuadTree(png, startX + height/2, startY, height/2);				// NE
			this.quarters[2] = new QuadTree(png, startX + height/2, startY + height/2, height/2);	// SE
			this.quarters[3] = new QuadTree(png, startX, startY + height/2, height/2);				// SW
			
		}
	}
	
	/**
	 * Default constructor.
	 * 
	 * @param png base image
	 */
	public QuadTree(ImagePNG png) {
		this(png, 0, 0, png.height());
		Compressor.deltaCompress(this, 0);
	}

	/**
	 * Leaf constructor.
	 * 
	 * @param c the color of the leaf
	 */
	public QuadTree(Color c) {
		this.col = c;
		this.quarters = new QuadTree[4];
		
		for(int i = 0; i < 4; i++) {
			this.quarters[i] = null;
		}
	}
	

	///////////////////////
	// Attribute getters //

	// FIXME should never be neccessary for good encapsulation
	public QuadTree[] getBranches() { return this.quarters; }

	//	Individual region getter
	public QuadTree getNW() { return this.quarters[0]; }
	public QuadTree getNE() { return this.quarters[1]; }
	public QuadTree getSE() { return this.quarters[2]; }
	public QuadTree getSW() { return this.quarters[3]; }

	//	Individual region setter
	public void setNW(QuadTree nw) { this.quarters[0] =  nw; }
	public void setNE(QuadTree ne) { this.quarters[1] =  ne; }
	public void setSE(QuadTree se) { this.quarters[2] =  se; }
	public void setSW(QuadTree sw) { this.quarters[3] =  sw; }

	/**
	 * If the color property is set, returns its value.
	 * 
	 * @return the color of the leaf node
	 * @throws IllegalArgumentException if the node is not a leaf
	 */
	public Color getColor() throws IllegalArgumentException {
		if(this.isLeaf()) {
			return this.col;
		} else {
			throw new IllegalArgumentException("Tree node is not a leaf.");
		}
	}

	/**
	 * Computes the average color of an end node from the color
	 * properties of its four leaves.
	 * 
	 * @return the average color
	 * @throws IllegalArgumentException if the node is not an end node
	 */
	public Color getAverageColor() throws IllegalArgumentException {
		if(this.isEndNode()) {
			Double[] avg = new Double[3];

			//	initializing the array
			for(int i = 0; i < 3; i++) {
				avg[i] = 0.0;
			}

			//	computing average RGB values
			for(int i = 0; i < 4; i++) {
				avg[0] += this.quarters[i].col.getRed()/4f;
				avg[1] += this.quarters[i].col.getGreen()/4f;
				avg[2] += this.quarters[i].col.getBlue()/4f;
			}

			return new Color(
				avg[0].intValue(), 
				avg[1].intValue(), 
				avg[2].intValue());
		} else {
			throw new IllegalArgumentException("Cannot compute average color of a non-end node.");
		}
	}


	/////////////////////////////////
	//	Inner structure operations //

	/**
	 * Depth-first search.
	 * 
	 * @return the list of colors in the tree, from left to right
	 */
	public ArrayList<Color> dfs() {
		ArrayList<Color> feuilles = new ArrayList<Color>();
		
		if(this.col != null) {
			feuilles.add(this.col);
		} else {
			for(int i= 0; i < this.quarters.length; i++) {
				feuilles.addAll(this.quarters[i].dfs());
			}
		}
		
		return feuilles;
	}


	/////////////////////////
	// Node discrimination //
	
	/**
	 * Due to how this structure is implemented, we know
	 * that if the col property is set, the tree cannot
	 * have branches: it is thus a leaf.
	 * 
	 * @return true if the node is a leaf node
	 */
	public boolean isLeaf() { return this.col != null; }

	/**
	 * A node is an "end node" if all of its direct
	 * successors are leaves.
	 * 
	 * @return true if the node is an end node
	 */
	public boolean isEndNode() { 
		boolean flag = true;

		for(int i = 0; i < 4; i++) {
			if(!this.quarters[i].isLeaf()) flag = false;
		}

		return flag;
	}


	////////////////////
	// Export methods //

	/**
	 * Converts the tree into a readable string.
	 */
	public String toString() {
		String s = "";
		
		if(this.col != null) {
			s = Integer.toHexString(this.col.getRGB()).substring(2);
		} else {
			for(int i = 0; i < 4; i++) {
				s += "(" + this.quarters[i].toString() + ")";
			}
		}
		
		return s;
	}

	/**
	 * Converts a QuadTree to a square PNG image.
	 * 
	 * @param side the height & width of the image
	 * @return a saveable bitmap
	 */
	public ImagePNG toPNG(int side) {
		ImagePNG img = new ImagePNG(side, side);
		this.paintColor(img, 0, 0, side);
		return img;
	}

	private void paintColor(ImagePNG img, int x, int y, int side) {
		if(this.isLeaf()) {
			System.out.println("Reached leaf, painting " + side*side + " pixels " + this.col + " from " + x + "x" + y);
			img.fillSquare(x, y, side, this.col);
		} else {
			this.quarters[0].paintColor(img, x, y, side/2);
			this.quarters[1].paintColor(img, x + side/2, y, side/2);
			this.quarters[2].paintColor(img, x + side/2, y + side/2, side/2);
			this.quarters[3].paintColor(img, x, y + side/2, side/2);
		}
	}
}
