# Square bitmap compressor

This tool can apply two kinds of image compression to a given square PNG image. It is a follow-up to a *Data Structures & Algorithms 3* project from my final (third) year of Licence Informatique at the Université de Nantes (France), which I carried out with Dimitri Allais.

## Examples 

### Delta compression

<img src="pngs/1024-cube.png" width="200" /> <img src="pngs/compressed.png" width="200" />

*Delta compression of 1024-cube with a compression factor of 90.*

On the example above, the file size was reduced from 656KB to 34KB, while the provided tool estimated the compression to be 99.27% faithful. With a factor of 20, the file size gets up to 46KB and is 99.88% faithful to the original.

The delta compression forces a level of cohesion within the image : given a certain threshold, it will merge together regions whose color is too "far away", in terms of euclidean distance, from their neighbors, so that all regions of the final image fit beneath the treshold. 

### Phi compression

*Work in progress...*

## Cloning & running

```
git clone https://gitlab.com/fsaget/bitmap-compression.git
cd bitmap-compression/src
javac *.java
java main
```

Made and tested with `openjdk11`.

## Roadmap

- [x] Import uni project
- [x] Un-french-ize method names and comments
- [ ] Rework code
    - [ ] Class organization
    - [x] Delta compression
    - [ ] Phi compression
    - [x] PNG export
    - [ ] Command-line interface
- [ ] Set up tests
- [ ] Create a nice GUI
- [ ] Add support for non-square images ?
- [ ] Add other compression algorithms ? 
